<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <link href="{{ mix('/css/app.css') }}" rel="stylesheet" />
        <link href="https://rsms.me/inter/inter.css" rel="stylesheet">

        <script src="https://polyfill.io/v3/polyfill.min.js?features=smoothscroll,NodeList.prototype.forEach,Promise,Object.values,Object.assign" defer></script>
        <script src="{{ mix('/js/app.js') }}" defer></script>
    </head>

    <body>
        @inertia
    </body>
</html>
